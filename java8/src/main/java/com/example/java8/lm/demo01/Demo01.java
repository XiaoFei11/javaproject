package com.example.java8.lm.demo01;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo01 {
    public static void main(String[] args) {
        // 定义字符串数组
        String[] strArr = { "abc", "cd", "abce", "a" };
        Arrays.sort(strArr, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.compare(o2.length(),o1.length());
            }
        });
        // 输出排序结果
        for (String s : strArr) {
            System.out.println(s);
        }
        System.out.println("---------------------");

        /*Arrays.sort(strArr, (o1,o2)->{
           return Integer.compare(o2.length(),o1.length());
        });
        for (String s : strArr) {
            System.out.println(s);
        }
        System.out.println("---------------------");*/

        /*new Thread(()->System.out.println(1+"hello world!!!")).start();

        // 方法体
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                System.out.println(2 + "hello world");
            }
        }).start();*/

        /*MyInterface myInterface=new MyInterface() {
            @Override
            public String info(String tip) {
                System.out.println("---------------");
                return "tip--->"+tip;
            }
        };
        String infoStr=myInterface.info("aaaaaaa1：");
        System.out.println(infoStr);*/

       /* MyInterface mm=(s)->{return s+"大飞哥";};
        System.out.println(mm.info("ha哈哈"));*/

        /*// 定义字符串
        String str = "helloworld";
        // 调用方法
        // 在调用的时候写方法体，方法比较灵活
        int a=testFun(str, (s)->{return s.length();});
        System.out.println("a--"+a);*/

        /*String str = "helloworld";
        testCon(str,(s)->{
            System.out.println("s--->"+s);
            System.out.println(s.length());
        });*/
        /*String ss=testSup(()->{return "aaaa";});
        //String ss=testSup(()->"aaaa");
        System.out.println("ss==="+ss);*/


        /*String str = "helloworld";
        boolean flag = testPre(str, (s)->s.length()==10);
        System.out.println(flag);*/

        //List list = Arrays.asList("aa","bb");

        /*list.forEach((s)->{
            System.out.println(s);
        });*/
        /*list.forEach((s)-> System.out.println(s));
        System.out.println("=========================");
        list.forEach(System.out::println);*/
        /*
        long l1=testAbs(-10, (s)->Math.abs(s));
        System.out.println(l1);
        long l2 = testAbs(-10,Math::abs);
        System.out.println(l2);

        List list = Arrays.asList("aa","bb");
        list.forEach(System.out::println);
        System.out.println("=================================");
        list.forEach((s)->System.out.printf("%s\n",s));*/


       /* // 创建集合
        List<String> list = new ArrayList<>();
        // 添加元素
        list.add("e");
        list.add("c");
        list.add("a");
        list.add("d");
        list.add("b");
        // 排序
        list.sort((s1, s2) -> s1.compareTo(s2));
        list.forEach((s) -> System.out.println(s));
        System.out.println("------------------------");
        list.forEach(System.out::println);*/
       /* getString(()->"aaab");
        getString(String::new);
        */


        /*// 创建集合
        List<String> list = new ArrayList<>();
        // 添加元素
        list.add("sdf");
        list.add("a");
        list.add("asdf");
        list.add("d");
        list.add("basdfgh");
        long a= list.stream().filter((s)->s.length()>2).count();
        System.out.println(a);
        List<String> list2 = list.stream().filter(s -> s.length() > 2).collect(Collectors.toList());
        list2.forEach(System.out::println);
        System.out.println("----------------");
        Set<String> set2 = list.stream().filter(s -> s.length() > 2).collect(Collectors.toSet());
        for (String s:set2){
            System.out.println(s);
        }
        System.out.println("----------------");
        set2.forEach(System.out::println);*/

       /* // 创建集合
        List<String> list = new ArrayList<>();

        // 添加元素
        list.add("sdf");
        list.add("a");
        list.add("asdf");
        list.add("d");
        list.add("basdfgh");

        List<String> collect = list.stream().map((s) -> s.toUpperCase()).collect(Collectors.toList());
        collect.forEach(System.out::println);
        System.out.println("========================");
        Map<String, String> collect1 = list.stream().map((s) -> s.toLowerCase())
                .collect(Collectors.toMap((a) -> a+"01", (b) -> b+"002"));

        collect1.forEach((s,t)-> System.out.println(s+"--"+t));*/

       /* // 创建集合
        List<String> list = new ArrayList<>();

        // 添加元素
        list.add("1sdf");
        list.add("a");
        list.add("2asdf");
        list.add("d");
        list.add("basdfgh");*/

        // 获取数字开头的
        //List<String> collect = list.stream().filter((s) -> s.startsWith("\\d")).collect(Collectors.toList());
        /*List<String> collect = list.stream().filter((s) -> Character.isDigit(s.charAt(0))).collect(Collectors.toList());
        collect.forEach(System.out::println);*/

        /*List<Boolean> collect = list.stream().flatMap((s) -> Stream.of(Character.isDigit(s.charAt(0)))).collect(Collectors.toList());
        collect.forEach(System.out::println);*/

       /* Stream<? extends Serializable> flatMap = Stream.of(Arrays.asList("a", "b"), Arrays.asList(1, 2, 3)).flatMap(s -> s.stream());
        flatMap.forEach(System.out :: println);

        Stream<? extends List<? extends Serializable>> listStream = Stream.of(Arrays.asList("a", "b"), Arrays.asList(1, 2, 3));
        listStream.forEach(System.out :: println);*/


        /*List<String> list = new ArrayList<>();

        list.add("abc");
        list.add("ab");
        list.add("abcd");
        list.add("abcde8899");*/

       /* // 获取最大值
        Optional<Integer> max = list.stream().map(s -> s.length()).max((s1, s2) -> {
            return s1.compareTo(s2);
        });
        Integer integer = max.get();
        System.out.println(integer);*/

        // 获取最大值
        /*Integer aa = list.stream().map(s -> s.length()).max((s1, s2) -> {
            return s1.compareTo(s2);
        }).get();
        System.out.println(aa);*/

        /*Integer aa = list.stream().map(s -> s.length()).max((s1, s2) -> {
            return s2.compareTo(s1);
        }).get();
        System.out.println(aa);*/

       /* // 获取最大值
        int max = list.stream().map((s) -> s.length()).max(Integer :: compareTo).get();
        System.out.println(max);


        // 获取最小值，另一种方法
        int min = list.stream().min(Comparator.comparing((s) -> s.length())).get().length();
        System.out.println(min);*/

        List<Long> list = new ArrayList<>();

        // 封装到集合
        for (long i = 1; i <= 100; i++) {
            list.add(i);
        }

        // 计算
        // reduce：参1，和的初始值
        Long reduce = list.stream().parallel().reduce(0L, (i, j) -> i + j);
        System.out.println(reduce);
    }
    //Supplier get	供货商; 供应者; 供货方;
    //Consumer apply	应用; 使用
    //Function accept 	同意; 收受; 接受(建议、邀请等)
    /**
     *
     * @param str 输入参数
     * @param fun 表达式 String 为输入类型，Integer为输出类型
     * @return 返回字符串长度
     */
    public static Integer testFun(String str, Function<String,Integer> fun){
        Integer apply = fun.apply(str);
        return apply;
    }
    public static void testCon(String str, Consumer<String> con){
        con.accept(str);
    }
    /**
     *
     * @param sup
     * @return
     */
    public static String testSup(Supplier<String> sup) {
        // 执行
        String s = sup.get();
        return s;
    }
    public static boolean testPre(String str, Predicate<String> pre){
       return pre.test(str);
    }
    public static long testAbs(long s, Function<Long, Long> fun) {
        Long l = fun.apply(s);
        return l;
    }

    public static void getString(Supplier<String> su) {
        String s = su.get();
        System.out.println("s---"+s);
        System.out.println(s == null);
    }
}
