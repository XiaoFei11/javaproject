package com.example.java8.lm.demo01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test1 {
    //a.获取Student集合中年龄小于20岁的集合中年龄最大的学生信息
    public static void main(String[] args) {
        /*List<Student> list = new ArrayList<>();
        Student s1 = new Student("张三", 21);
        Student s2 = new Student("李四", 19);
        Student s3 = new Student("王五", 18);
        Student s4 = new Student("程六", 17);
        Student s5 = new Student("赵七", 20);
        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(s4);
        list.add(s5);*/

      /*  Student student = list.stream().filter((s) -> s.getAge() < 20).max((ss1, ss2) -> ss1.getAge().compareTo(ss2.getAge())).get();
        System.out.println(student.getAge());*/

        // 筛选
        /*Student student = list.stream().filter((s) -> s.getAge() < 20).max(Comparator.comparing((s) -> s.getAge()))
                .get();
        System.out.println(student);*/
        /*
        List<Student> collect = list.stream().filter((s) -> s.getAge() < 20).collect(Collectors.toList());
        System.out.println(collect);*/

       /* //b.查找集合中以a开头的字符的长度集合
        List<Student> collect = list.stream().filter((s) -> s.getName().charAt(0) == 'a').collect(Collectors.toList());
        System.out.println(collect.size());*/
        /*List<Integer> collect = Stream.of("abc", "b", "a", "abcd").filter((s) -> s.startsWith("a")).map((s)->{return s.length();}).collect(Collectors.toList());
        System.out.println(collect.size());*/

        List<Long> list = new ArrayList<>();

        // 封装到集合
        for (long i = 1; i <= 100; i++) {
            list.add(i);
        }

        // 计算
        // reduce：参1，和的初始值
        long l1 = list.stream().parallel().reduce(0L, (s, l) -> s + l).longValue();
        System.out.println(l1);
    }
}
