package com.example.java8.lm.demo01;

import javax.lang.model.util.SimpleAnnotationValueVisitor6;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateTest {
    public static void main(String[] args) {
        /*Instant now = Instant.now();
        System.out.println(now);

        // 获取当前时间
        Instant instant = Instant.ofEpochMilli(new Date().getTime());
        System.out.println(instant);

        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));*/

       /* // 获取当前时间
        Instant instant = Instant.parse("1993-02-06T10:12:35Z");
        System.out.println(instant);*/

        /*// 当前时间
        LocalDate now = LocalDate.now();
        System.out.println(now);

        // 往前推两天
        LocalDate date = now.minusDays(2);
        System.out.println(date);

        LocalDate localDate1 = now.plusDays(2);
        System.out.println(localDate1);

        // 制定一个日期
        LocalDate localDate = LocalDate.of(2021, 2, 6);
        System.out.println(localDate);*/

       /* // 当前时间
        LocalTime now = LocalTime.now();
        System.out.println(now);

        // 22:33
        LocalTime localTime = LocalTime.of(22, 33);
        System.out.println(localTime);

        // 一天中的4503秒
        //LocalTime ofDay = LocalTime.ofSecondOfDay(4503);
        LocalTime ofDay = LocalTime.ofSecondOfDay(200);

        System.out.println(ofDay);*/

        // 当前时间
       /* LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        // 当前时间加上25小时３分钟
        LocalDateTime plusMinutes = now.plusHours(25).plusMinutes(3);
        System.out.println(plusMinutes);

        // 转换
        LocalDateTime of = LocalDateTime.of(1993, 2, 6, 11, 23, 30);
        System.out.println(of);*/

        /*// 当前时间
        ZonedDateTime now = ZonedDateTime.now();
        System.out.println(now);

        // 创建时区的日期时间对象
        ZonedDateTime of = ZonedDateTime.of(LocalDateTime.of(1993, 2, 6, 11, 23, 30), ZoneId.of("+08"));
        System.out.println(of);*/

       /* // 格式化
        String time = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss").format(LocalDateTime.now());
        System.out.println(time);*/
        // 格式化
        /*LocalDateTime parse = LocalDateTime.parse("2017.01.01 08:08:08", DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss"));
        System.out.println(parse);

        Instant timestamp = new Date().toInstant();

        Instant instant = Calendar.getInstance().toInstant();

        System.out.println(timestamp);
        System.out.println(instant);*/

        String[] str = {"a", "b", "c"};

        String join = String.join("-", str);
        System.out.println(join);

    }
}
