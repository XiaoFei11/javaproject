package com.thread.p1;

public class T3 {
    private long a=0;

    public long getA() {
        return a;
    }

    public void setA(long a) {
        this.a = a;
    }

    public static void main(String[] args) {
        T3 t3=new T3();
        for (int i=0;i<100000;i++){
            Thread t1=new Thread(new Runnable() {
                @Override
                public void run() {
                    t3.setA(100);
                }
            });
            Thread t2=new Thread(new Runnable() {
                @Override
                public void run() {
                    //System.out.println("---------------");
                    if(t3.getA()!=100){
                        System.out.println("t3.getA!=100----->"+t3.getA());
                    }

                }
            });
            t1.start();
            t2.start();
        }
        System.out.println("main..end...");

    }
}
