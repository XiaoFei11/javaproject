package com.thread.p1;

public class T1 {
    public static void main(String[] args) {
        System.out.println("mainenter....");
        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    System.out.println("t1 executing....");
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t1.setDaemon(true);//以守护线程运行,而且必须放到start()前面才生效.这样main结束 t1也结束，，否则t1不结束。
        t1.start();
        System.out.println("main end ....");

    }
}
