package com.thread.p1;

public class T2 {
    private final static Object lock=new Object();

    public static void main(String[] args)  {
       /* Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lock){
                    try {
                        Thread.sleep(20000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t1.start();*/
        /*System.out.println("------------------------------");
        MyThread t=new MyThread(lock);
        t.start();
        try {
            Thread.sleep(500);
            System.out.println("t1-------->"+t.isInterrupted());
            t.interrupt();
            System.out.println("t2-------->"+t.isInterrupted());
            t.join();
            System.out.println("t3-------->"+t.isInterrupted());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(t.isInterrupted());
        //t.stop1();
        //t.join();
        System.out.println("main end ....");*/


        A a=new A();
        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                a.f1();
            }
        });
        Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                a.f2();
            }
        });
        t1.start();
        t2.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("main---end....");
    }
}

class MyThread extends Thread{
    private boolean stopped=false;
    private Object lock=null;
    public MyThread(Object lock){
        this.lock=lock;
    }
    @Override
    public void run() {
        while (!stopped){
            //System.out.println("Thread.interrupted()----------1----------"+Thread.interrupted());
            /*try {
                Thread.sleep(1000);
                //System.out.println("Thread.interrupted()-----------2---------"+Thread.interrupted());
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }*/
            //System.out.println("Thread.interrupted()-----------3---------"+Thread.interrupted());
            /*if(Thread.interrupted()){
                System.out.println("Thread.interrupted().................");
                System.out.println("Thread.interrupted()==="+Thread.interrupted());
            }*/
            /*try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
                stopped=true;
            }*/
            /*if(Thread.interrupted()){
                System.out.println("interrupted............................");
            }*/
            /*try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            /*synchronized (lock){
                System.out.println("block..................");
            }*/
            //System.out.println("block end...................");
        }
    }
    public  void stop1(){
        this.stopped=true;
    }
}
class A{
    /*public synchronized void f1(){
    }
    public static synchronized void f2(){
    }*/

    /*public  void f1(){
        synchronized (this){
        }
    }
    public static  void f2(){
        synchronized (A.class){}
    }*/

    public synchronized void f1(){
        System.out.println(Thread.currentThread().getName()+"---f1--start>");
        try {
            this.wait();
            System.out.println(Thread.currentThread().getName()+"---f1-wait-end>");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"---f1--end>");
    }
    public  synchronized void f2(){
        System.out.println(Thread.currentThread().getName()+"---f2--start>");
        this.notify();
        System.out.println(Thread.currentThread().getName()+"---f2--end>");
    }
}
