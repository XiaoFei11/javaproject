package com.thread.p2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Example {
    private int count=0;

    public synchronized void increment(){
        count++;
    }
    public synchronized void decrement(){
        count--;
    }
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    private AtomicInteger counter=new AtomicInteger(0);
    private void add(){
        //counter.getAndSet(1);
        //counter.getAndIncrement();
        counter.getAndAdd(1);
    }
    private void decr(){
        //counter.getAndSet(-1);
        //counter.getAndDecrement();
        counter.getAndAdd(-1);
    }
    public int getCounterValue(){
        return counter.get();
    }

    public static void main(String[] args) {
        /*Example example=new Example();
        for (int i=0;i<1000;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    example.increment();
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        for (int i=0;i<1000;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    example.decrement();
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int a=example.getCount();
        System.out.println(a);*/

        List<Thread> list=new ArrayList<>();
        Example example=new Example();
        for (int i=0;i<100;i++){
            Thread t=new Thread(new Runnable() {
                @Override
                public void run() {
                    example.add();
                }
            });
            t.start();
            list.add(t);
        }
        List<Thread> list2=new ArrayList<>();
        for (int i=0;i<100;i++){
            Thread t2=new Thread(new Runnable() {
                @Override
                public void run() {
                    example.decr();
                }
            });
            t2.start();
            list2.add(t2);
        }
        /*try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        for (int i=0;i<list.size();i++){
            Thread t=list.get(i);
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i=0;i<list2.size();i++){
            Thread t=list2.get(i);
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("-------------");
        int a=example.getCounterValue();
        System.out.println(a);

    }
}
