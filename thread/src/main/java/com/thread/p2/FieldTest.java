package com.thread.p2;

public class FieldTest {
    protected volatile int num1;

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public void add(){
        num1++;
    }
    public void decr(){
        num1--;
    }
}
