package com.thread.p2;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicStampedReference;

public class T1 {
    public static void main(String[] args) {
        /*Example example=new Example();
        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                example.setCount(100);
            }
        });
        t1.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Example example2=new Example();
        Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(example2.getCount());
            }
        });
        t2.start();*/
        FieldTest fieldTest=new FieldTest();

        //AtomicStampedReference atomicStampedReference=new AtomicStampedReference(1,1);
        AtomicIntegerFieldUpdater<FieldTest> num1Updater = AtomicIntegerFieldUpdater.newUpdater(FieldTest.class, "num1");
        int num1=num1Updater.incrementAndGet(fieldTest);
        System.out.println(num1);

    }
}
